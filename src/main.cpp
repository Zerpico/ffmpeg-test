#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif

#define DllExport __attribute__((visibility("default")))
#define DllImport
#define CALL

extern "C"
{
	#include "libavcodec/avcodec.h"
	#include "libavformat/avformat.h"
}

#include <iostream>
#include <cstdlib>

using namespace std;


int main()
{
	auto conf = avcodec_configuration();
	cout << conf << endl;
    return 0;
}
